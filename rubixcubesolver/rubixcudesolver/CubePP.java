package rubixcudesolver;

import java.util.LinkedList;
import java.util.List;

/**
 * A further development of the cube. This has a couple of more methods
 * implemented as an api. It also has history of Movements.
 * 
 * @author Mats
 *
 */
public class CubePP extends Cube {
	LinkedList<Integer> movements = new LinkedList<>();

	public CubePP() {
		super();
	}

	public CubePP(int index) {
		super(index);
	}

	@Override
	public void twist(int twist) {
		// TODO Auto-generated method stub
		super.twist(twist);
		movements.add(twist);
	}

	public void stepBack() {
		int movement = movements.pollLast();
		movement = Cube.oppositeMove(movement);
		super.twist(movement);
	}

	private int lastMove() {
		Integer lastMove = movements.peekLast();
		return lastMove == null ? -1 : lastMove;
	}

	/**
	 * Checks if all conditions in the list is fullfulled
	 * @param cond
	 * @return
	 */
	private boolean allConditions(List<Condition> cond){
		boolean b = true;
		for (Condition condition : cond) {
			b = b && condition.check(this);
		}
		return b;
	}
	
	/**
	 * This method tries all combinations of moves until the conditions for the
	 * Cube is true
	 * 
	 * @param level
	 * @param stop
	 * @param cond
	 * @return
	 */
	public boolean tryAllMoves(int level, int stop, List<Condition> cond) {
		// If condition if fullfilled return true
		

		if(allConditions(cond)){
			return true;
		}

		// If we've gone to deep in the recursion, return false
		if (level == stop) {
			return false;
		}

		// Try all moves
		for (int i = 0; i < 12; i++) {
			// We don't want to twist back.
			if (lastMove() == oppositeMove(i)) {
				continue;
			}
			twist(i);
			boolean b = tryAllMoves(level + 1, stop, cond);
			// If b is true, return!
			if (b) {
				return true;
			}

			// Otherwise, backtrack and try other conbinations
			stepBack();
		}

		// Solution could not be found!
		return false;
	}
	
	private String movementsToString(){
		LinkedList<String> movementsNames = new LinkedList<>();
		
		for (int movement : movements) {
			movementsNames.add(movementName(movement));
		}
		return movementsNames.toString();
	}
	
	public String toString() {
		return movementsToString() + "\n" + super.toString();
	}

}
