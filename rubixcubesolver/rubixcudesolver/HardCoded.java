package rubixcudesolver;

import java.io.File;

public class HardCoded {
	public final static String SEP = File.separator;
	public final static String CUBES_PATH = "rubixcubesolver/rubixcudesolver/cubes.txt".replace("/", SEP);
}
