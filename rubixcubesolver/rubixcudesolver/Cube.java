package rubixcudesolver;

import static rubixcudesolver.HardCoded.CUBES_PATH;
import static rubixcudesolver.Utilities.*;

import javax.print.attribute.standard.Sides;

public class Cube {
	Edge side[] = new Edge[6];

	private static final int BACK = 0;
	private static final int TOP = 1;
	private static final int RIGHT = 2;
	private static final int FRONT = 3;
	private static final int LEFT = 4;
	private static final int BOTTOM = 5;

	public static final int BACK_COUNTER = BACK * 2;
	public static final int BACK_CLOCK = BACK * 2 + 1;
	public static final int TOP_COUNTER = TOP * 2;
	public static final int TOP_CLOCK = TOP * 2 + 1;
	public static final int RIGHT_COUNTER = RIGHT * 2;
	public static final int RIGHT_CLOCK = RIGHT * 2 + 1;
	public static final int FRONT_COUNTER = FRONT * 2;
	public static final int FRONT_CLOCK = FRONT * 2 + 1;
	public static final int LEFT_COUNTER = LEFT * 2;
	public static final int LEFT_CLOCK = LEFT * 2 + 1;
	public static final int BOTTOM_COUNTER = BOTTOM * 2;
	public static final int BOTTOM_CLOCK = BOTTOM * 2 + 1;

	public Cube() {

		Edge y = initSide('y');
		Edge b = initSide('b');
		Edge o = initSide('o');
		Edge w = initSide('w');
		Edge r = initSide('r');
		Edge g = initSide('g');

		connect(w, 0, b, 2);
		connect(w, 1, r, 3);
		connect(w, 2, g, 0);
		connect(w, 3, o, 1);

		connect(b, 0, y, 2);
		connect(b, 1, r, 0);
		connect(b, 3, o, 0);

		connect(y, 0, g, 2);
		connect(y, 1, r, 1);
		connect(y, 3, o, 3);

		connect(g, 1, r, 2);
		connect(g, 3, o, 2);

		side[BACK] = y;
		side[TOP] = b;
		side[RIGHT] = o;
		side[FRONT] = w;
		side[LEFT] = r;
		side[BOTTOM] = g;

	}

	public Cube(int index) {
		this();
		readCubeFromFile(index);
	}

	private int cross(Edge edge) {
		int counter = 0;

		Mid front = edge.mid;

		for (int i = 0; i < front.edge.length; i++) {
			if (front.edge(i).c != front.c) {
				continue;
			}
			if (front.edge(i).edge.c != front.edge(i).edge.mid.c) {
				continue;
			}
			counter++;
		}
		return counter;
	}

	private int corners(Edge edge) {
		int counter = 0;

		Mid front = edge.mid;

		for (int i = 0; i < front.edge.length; i++) {
			Corner corner = front.edge(i).corner[0];
			if (corner.c != front.c) {
				continue;
			}
			if (corner.corner[0].c != corner.corner[0].mid().c) {
				continue;
			}

			if (corner.corner[1].c != corner.corner[1].mid().c) {
				continue;
			}
			counter++;
		}

		return counter;

	}

	public int cross() {
		return cross(side[FRONT]);
	}

	public int corners() {
		return corners(side[FRONT]);

	}

	public int sides() {
		int counter = 0;

		Mid front = side[FRONT].mid;

		for (int i = 0; i < front.edge.length; i++) {
			Edge edge = front.edge(i).corner[0].corner[0].edge[0];

			if (edge.c != edge.mid.c) {
				continue;
			}
			if (edge.edge.c != edge.edge.mid.c) {
				continue;
			}
			counter++;
		}

		return counter;
	}

	public int backCross() {
		return cross(side[BACK]);
	}

	public int backCorners() {
		return corners(side[BACK]);

	}

	public void shuffle() {
		for (int i = 0; i < 200; i++) {
			int twist = (int) (12 * Math.random());
			twist(twist);
		}
	}

	public void tests() {

		Cube cube = new Cube(1);
		System.out.println(cube.toString());
		System.out.println(cube.backCross());
		System.out.println(Cube.oppositeMove(BOTTOM_CLOCK));
		// System.out.println(cube.toString());


	}

	private void setCube(char[][] cube) {
		side[FRONT].getBack().setSide(sub(cube, 0, 3, 3, 6));
		side[FRONT].getTop().setSide(sub(cube, 3, 6, 3, 6));
		side[FRONT].getLeft().setSide(sub(cube, 6, 9, 0, 3));
		side[FRONT].setSide(sub(cube, 6, 9, 3, 6));
		side[FRONT].getRight().setSide(sub(cube, 6, 9, 6, 9));
		side[FRONT].getBottom().setSide(sub(cube, 9, 12, 3, 6));
	}

	/**
	 * Reads the given incdex of the cube from the cubes.txt file
	 * 
	 * @param index
	 */
	public void readCubeFromFile(int index) {

		String string = readFromFile(CUBES_PATH);
		StringBuilder sb = new StringBuilder();

		for (String s : string.split("\n")) {
			if (s.trim().length() == 0) {
				continue;
			}
			if (s.trim().charAt(0) == '#') {
				continue;
			}
			sb.append(s + "\n");
		}

		string = sb.toString();
		string = string.toLowerCase();

		int idx1 = string.indexOf("cube " + index + ":");
		int idx2 = string.indexOf("cube " + (index + 1) + ":");

		string = idx2 == -1 ? string.substring(idx1) : string.substring(idx1,
				idx2);
		string = string.substring(string.indexOf(":") + 1).trim();

		string = "      " + string.substring(string.indexOf(":") + 1).trim();

		String rows[] = string.split("\n");

		char[][] cube = new char[12][9];
		for (int i = 0; i < cube.length; i++) {
			for (int j = 0; j < cube[i].length; j++) {
				cube[i][j] = ' ';
			}
		}

		for (int i = 0; i < 6; i++) {
			String cols[] = rows[i].trim().split(" ");
			for (int j = 0; j < cols.length; j++) {
				cube[i][j + 3] = cols[j].charAt(0);
			}
		}

		for (int i = 6; i < 9; i++) {
			String cols[] = rows[i].trim().split(" ");
			for (int j = 0; j < cols.length; j++) {
				cube[i][j] = cols[j].charAt(0);
			}
		}
		for (int i = 9; i < 12; i++) {
			String cols[] = rows[i].trim().split(" ");
			for (int j = 0; j < cols.length; j++) {
				cube[i][j + 3] = cols[j].charAt(0);
			}
		}
		setCube(cube);
	}

	public void twist(int twist) {
		if (twist % 2 == 0) {
			counter(side[twist / 2]);
		} else {
			clock(side[twist / 2]);
		}
	}

	public static int oppositeMove(int twist) {
		if (twist % 2 == 0) {
			return twist + 1;
		}
		return twist - 1;
	}

	/**
	 * Flips the cube front side clockwise.
	 */
	private void clock(Edge edge) {

		Mid front = edge.mid;

		char c1 = front.edge(0).c;
		char c2 = front.edge(0).edge.c;
		char c3 = front.edge(0).corner[0].c;
		char c4 = front.edge(0).corner[0].corner[0].c;
		char c5 = front.edge(0).corner[0].corner[1].c;

		for (int i = front.edge.length - 1; i >= 0; i--) {
			front.edge(i + 1).c = front.edge(i).c;
			front.edge(i + 1).edge.c = front.edge(i).edge.c;
			front.edge(i + 1).corner[0].c = front.edge(i).corner[0].c;
			front.edge(i + 1).corner[0].corner[0].c = front.edge(i).corner[0].corner[0].c;
			front.edge(i + 1).corner[0].corner[1].c = front.edge(i).corner[0].corner[1].c;

		}
		front.edge(1).c = c1;
		front.edge(1).edge.c = c2;
		front.edge(1).corner[0].c = c3;
		front.edge(1).corner[0].corner[0].c = c4;
		front.edge(1).corner[0].corner[1].c = c5;
	}

	/**
	 * Flips the cube front side counter-clockwise.
	 */
	private void counter(Edge edge) {

		Mid front = edge.mid;

		char c1 = front.edge(0).c;
		char c2 = front.edge(0).edge.c;
		char c3 = front.edge(0).corner[0].c;
		char c4 = front.edge(0).corner[0].corner[0].c;
		char c5 = front.edge(0).corner[0].corner[1].c;

		for (int i = 0; i < front.edge.length; i++) {
			front.edge(i).c = front.edge(i + 1).c;
			front.edge(i).edge.c = front.edge(i + 1).edge.c;
			front.edge(i).corner[0].c = front.edge(i + 1).corner[0].c;
			front.edge(i).corner[0].corner[0].c = front.edge(i + 1).corner[0].corner[0].c;
			front.edge(i).corner[0].corner[1].c = front.edge(i + 1).corner[0].corner[1].c;

		}
		front.edge(3).c = c1;
		front.edge(3).edge.c = c2;
		front.edge(3).corner[0].c = c3;
		front.edge(3).corner[0].corner[0].c = c4;
		front.edge(3).corner[0].corner[1].c = c5;
	}

	/**
	 * Connects two sides. Used when initialising the cube.
	 * 
	 * @param first
	 * @param i
	 * @param second
	 * @param j
	 */
	private void connect(Edge first, int i, Edge second, int j) {
		Mid mFirst = first.mid;
		Mid mSecond = second.mid;

		mFirst.edge(i).edge = mSecond.edge(j);
		mSecond.edge(j).edge = mFirst.edge(i);

		Corner bLeft = mFirst.edge(i).corner[0];
		Corner bRight = mFirst.edge(i).corner[1];

		Corner aLeft = mSecond.edge(j).corner[1];
		Corner aRight = mSecond.edge(j).corner[0];

		bLeft.corner[1] = aLeft;
		bRight.corner[0] = aRight;

		aLeft.corner[0] = bLeft;
		aRight.corner[1] = bRight;

	}

	private Edge initSide(char c) {
		Mid mid = new Mid(c);
		Edge[] edges = new Edge[4];
		Corner[] corners = new Corner[4];
		for (int i = 0; i < edges.length; i++) {
			edges[i] = new Edge(c);
			edges[i].mid = mid;
			mid.edge[i] = edges[i];
		}
		for (int i = 0; i < corners.length; i++) {
			corners[i] = new Corner(c);
			corners[i].edge[0] = edges[i % 4];
			corners[i].edge[1] = edges[(i + 1) % 4];

			corners[i].edge[0].corner[1] = corners[i];
			corners[i].edge[1].corner[0] = corners[i];
		}
		return mid.edge(0);
	}

	private class Corner {
		private char c;
		private Corner[] corner = new Corner[2];
		private Edge[] edge = new Edge[2];

		public Corner(char c) {
			this.c = c;
		}

		public Mid mid() {
			return edge[0].mid;
		}

	}

	private class Edge {
		private char c;
		private Edge edge;
		private Mid mid;
		private Corner[] corner = new Corner[2];

		public Edge(char c) {
			this.c = c;
		}

		public Edge getTop() {
			return edge.corner[1].edge[1].corner[1].edge[1];
		}

		public Edge getBack() {
			return getTop().edge.corner[1].edge[1].corner[1].edge[1];
		}

		public Edge getLeft() {
			return corner[0].corner[0].edge[0];
		}

		public Edge getRight() {
			return corner[1].corner[1].edge[1];
		}

		public Edge getBottom() {
			return corner[0].edge[0].corner[0].edge[0].edge;
		}

		public char[][] toCharMatrx() {
			char[][] cube = new char[12][9];
			for (int i = 0; i < cube.length; i++) {
				for (int j = 0; j < cube[i].length; j++) {
					cube[i][j] = ' ';
				}
			}

			Edge edge = this;
			Edge top = getTop();
			Edge back = getBack();

			Edge bottom = getBottom();
			Edge left = getLeft();
			Edge right = getRight();

			append(getSide(back), cube, 0, 3);
			append(getSide(top), cube, 3, 3);
			append(getSide(left), cube, 6, 0);
			append(getSide(edge), cube, 6, 3);
			append(getSide(right), cube, 6, 6);
			append(getSide(bottom), cube, 9, 3);
			return cube;
		}

		public void setSide(char[][] side) {
			corner[0].c = side[0][0];
			c = side[0][1];
			corner[1].c = side[0][2];

			corner[0].edge[0].c = side[1][0];
			mid.c = side[1][1];
			corner[1].edge[1].c = side[1][2];

			corner[0].edge[0].corner[0].c = side[2][0];
			corner[0].edge[0].corner[0].edge[0].c = side[2][1];
			corner[1].edge[1].corner[1].c = side[2][2];
		}

		private char[][] getSide(Edge edge) {
			char c[][] = new char[3][3];

			c[0][0] = edge.corner[0].c;
			c[0][1] = edge.c;
			c[0][2] = edge.corner[1].c;

			c[1][0] = edge.corner[0].edge[0].c;
			c[1][1] = edge.mid.c;
			c[1][2] = edge.corner[1].edge[1].c;

			c[2][0] = edge.corner[0].edge[0].corner[0].c;
			c[2][1] = edge.corner[0].edge[0].corner[0].edge[0].c;
			c[2][2] = edge.corner[1].edge[1].corner[1].c;

			return c;
		}

		public Edge edge(int i) {
			return mid.edge(i);
		}

		public String toString() {

			return Utilities.toString(toCharMatrx());

		}

	}

	private class Mid {
		private char c;
		private Edge[] edge = new Edge[4];

		private Mid(char c) {
			this.c = c;
		}

		private Edge edge(int i) {
			return edge[(i + 4) % 4];
		}

	}

	public String toString() {
		return side[FRONT].toString();
	}

	public static String movementName(int twist) {

		String moves[] = new String[12];

		moves[BACK_COUNTER] = "BACK_COUNTER";
		moves[BACK_CLOCK] = "BACK_CLOCK";
		moves[TOP_COUNTER] = "TOP_COUNTER";
		moves[TOP_CLOCK] = "TOP_CLOCK";
		moves[RIGHT_COUNTER] = "RIGHT_COUNTER";
		moves[RIGHT_CLOCK] = "RIGHT_CLOCK";
		moves[FRONT_COUNTER] = "FRONT_COUNTER";
		moves[FRONT_CLOCK] = "FRONT_CLOCK";
		moves[LEFT_COUNTER] = "LEFT_COUNTER";
		moves[LEFT_CLOCK] = "LEFT_CLOCK";
		moves[BOTTOM_COUNTER] = "BOTTOM_COUNTER";
		moves[BOTTOM_CLOCK] = "BOTTOM_CLOCK";

		return moves[twist];
	}

	public static void main(String[] args) {
		double d = System.currentTimeMillis();
		new Cube().tests();
		;
		System.err.println("Time: " + (System.currentTimeMillis() - d));
	}
}
