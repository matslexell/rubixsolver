package rubixcudesolver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import static rubixcudesolver.HardCoded.*;

public class Utilities {
	
	public static void append(char[][] small, char[][] big, int row, int col){
		for (int i = 0; i < small.length; i++) {
			for (int j = 0; j < small[i].length; j++) {
				big[i+row][j+col] = small[i][j];
			}
		}
	}
	
	public static String print(char[][] c){
		String s = toString(c);
		System.out.println(s);
		return s;
	}
	
	public static String toString(char[][] c){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				sb.append(c[i][j] + " ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
//	public static char[][] readCube(int idx){
//		
//	}
	
	public static String readFromFile(String path) {



        File file = new File(path);
        Scanner s;
        String content = "";
        try {
            s = new Scanner(file);
            content = s.useDelimiter("\\Z").next();
            s.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace(); // See comment about logging framework
        }
        return content;
    }
	
	
	public static char[][] sub(char[][] mtx, int rFrom, int rTo, int cFrom, int cTo){
		char[][] c = new char[rTo-rFrom][cTo-cFrom];
		
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[i].length; j++) {
				c[i][j] = mtx[i+rFrom][j+cFrom];
			}
		}
		return c;
	}
	
	public static void printMatrixAsCsv(String[][] s){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length; i++) {
			for (int j = 0; j < s[i].length; j++) {
				
				sb.append(s[i][j]);
				if(j +1 == s[i].length){
					continue;
				}
				sb.append(",");
			}
			sb.append("\n");
		}
		System.out.println(sb.toString());
	}
	
	public static void main(String[] args) {


		
		
	}
	
}
