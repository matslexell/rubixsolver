package rubixcudesolver;

public abstract class Condition {
	private int n;

	public void setN(int n){
		this.n = n;
	}
	
	public int getN(){
		return n;
	}
	public abstract boolean check(Cube cube);
}
