package rubixcudesolver;

import java.util.LinkedList;
import java.util.List;

public class Solver {
	public Solver() {

	}

	public void init() {
		CubePP cube = new CubePP(2);
		if(solve(cube)){
			System.out.println(cube);
		} else {
			System.out.println("No solution found...");
		}
	}
	
	public boolean solve(CubePP cube){
		boolean b = true;
		LinkedList<Condition> conds = new LinkedList<Condition>();
		b = b && solveLayer(cube,4,new Cross(), conds);
		b = b && solveLayer(cube,6,new Corners(),conds);
//		b = b && solveLayer(cube,7,new Sides(),conds);
//		b = b && solveLayer(cube,4,new BackCross(),conds);
//		b = b && solveLayer(cube,8,new BackCorners(),conds);
	
		
		return b;
	}
	
	public boolean solveLayer(CubePP cube, int level,Condition cond, LinkedList<Condition> conds ){
		conds.add(cond);
		boolean b = true;
		for (int i = 0; i < 4; i++) {
			conds.peekLast().setN(i+1);
			b = b && cube.tryAllMoves(0, level, conds);
		}
		return b;
	}

	public class Cross extends Condition {



		@Override
		public boolean check(Cube cube) {
			return cube.cross() == getN();
		}

	}
	
	public class Corners extends Condition {


		@Override
		public boolean check(Cube cube) {
			return cube.corners() == getN();
		}

	}
	
	public class Sides extends Condition {

		@Override
		public boolean check(Cube cube) {
			return cube.sides() == getN();
		}

	}
	
	public class BackCross extends Condition {

		@Override
		public boolean check(Cube cube) {
			return cube.backCross()== getN();
		}

	}
	
	public class BackCorners extends Condition {

		@Override
		public boolean check(Cube cube) {
			// TODO Auto-generated method stub
			return cube.backCorners() == getN();
		}

		

	}

	public static void main(String[] args) {
		new Solver().init();
	}
}
