package rubixcudesolver;

public class Cube_OLD {
	final static int BACK = 0;
	final static int TOP = 1;
	final static int LEFT = 2;
	final static int FRONT = 3;
	final static int RIGHT = 4;
	final static int BOTTOM = 5;
	
	private char cube[][] = new char[12][9];
	private static Twister twister;
	private static int[] counter = { 1, 0, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10 };

	public Cube_OLD() {
		char[] side = { ' ', 'y', ' ', ' ', 'b', ' ', 'o', 'w', 'r', ' ', 'g',
				' ' };

		for (int row = 0; row < 12; row++) {
			for (int col = 0; col < 9; col++) {
				cube[row][col] = ' ';
			}
		}

		for (int i = 0, idx = 0; i < 4; i++) {
			for (int j = 0; j < 3; j++, idx++) {
				initiateSide(side[idx], i * 3, j * 3);
				// initiateSideRandom(side[idx],i * 3, j * 3);
			}
		}

		twister = new Twister();

	}

	private Cube_OLD(char cube[][]) {
		this.cube = cube;
	}

	public Cube_OLD clone() {
		
		char cube[][] = new char[12][9];
		for (int i = 0; i < cube.length; i++) {
			for (int j = 0; j < cube[i].length; j++) {
				cube[i][j] = this.cube[i][j];
			}
		}
		return new Cube_OLD(cube);
		
	}

	public void twist(int i) {
		twister.twist(i);
	}

	public int counterMove(int i) {
		// return (i/2)*2 + (i%2 == 0 ? 1 : 0 );
		return counter[i];
	}

	private void initiateSide(char side, int x, int y) {
		for (int row = x; row < x + 3; row++) {
			for (int col = y; col < y + 3; col++) {
				cube[row][col] = side;
			}
		}
	}

	private void initiateSideRandom(char side, int x, int y) {
		for (int row = x; row < x + 3; row++) {
			for (int col = y; col < y + 3; col++) {
				if (side == ' ')
					cube[row][col] = side;
				else
					cube[row][col] = (char) ((Math.random() * 26) + 65);
			}
		}
	}

	public boolean isSolved() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 3; j++) {
				if (!sideIsSolved(i * 3, j * 3))
					return false;
			}
		}
		return true;
	}

	private boolean sideIsSolved(int row, int col) {
		char side = cube[row][col];
		for (int i = row; i < 3 + row; i++) {
			for (int j = col; j < 3 + col; j++) {
				if (cube[i][j] != side)
					return false;
			}
		}
		return true;
	}

	public String toString() {
		String string = "";
		for (int row = 0; row < 12; row++) {
			for (int col = 0; col < 9; col++) {
				string = string + cube[row][col] + " ";
			}
			string = string + "\n";
		}
		return string;
	}

	public String toStringCoordinates() {
		String string = "";
		for (int row = 0; row < 12; row++) {
			for (int col = 0; col < 9; col++) {
				if (cube[row][col] == ' ')
					string = string + "     ";
				else
					string = string + "(" + row + "," + col + ")";
			}
			string = string + "\n";
		}
		return string;
	}

	private char[] getVertSide(int row, int col) {
		char side[] = new char[3];
		for (int i = 0; i < 3; i++)
			side[i] = cube[row + i][col];

		return side;
	}

	private char[] getHorSide(int row, int col) {
		char side[] = new char[3];
		for (int i = 0; i < 3; i++)
			side[i] = cube[row][col + i];
		return side;
	}

	private void setHorSide(int row, int col, char[] side) {
		for (int i = 0; i < 3; i++)
			cube[row][col + i] = side[i];
	}

	private void setHorSideCounter(int row, int col, char[] side) {
		for (int i = 0; i < 3; i++)
			cube[row][col + i] = side[2 - i];
	}

	private void setVertSide(int row, int col, char[] side) {
		for (int i = 0; i < 3; i++)
			cube[row + i][col] = side[i];
	}

	private void setVertSideCounter(int row, int col, char[] side) {
		for (int i = 0; i < 3; i++)
			cube[row + i][col] = side[2 - i];
	}

	private void turnMiddleClock(int row, int col) {
		char[] above = getHorSide(row - 1, col - 1);
		char[] under = getHorSide(row + 1, col - 1);
		char left = cube[row][col - 1];
		char right = cube[row][col + 1];

		cube[row - 1][col] = left;
		cube[row + 1][col] = right;

		setVertSide(row - 1, col + 1, above);
		setVertSide(row - 1, col - 1, under);

	}

	private void turnMiddleCounter(int row, int col) {
		char[] above = getHorSide(row - 1, col - 1);
		char[] under = getHorSide(row + 1, col - 1);
		char left = cube[row][col - 1];
		char right = cube[row][col + 1];

		cube[row - 1][col] = right;
		cube[row + 1][col] = left;

		setVertSideCounter(row - 1, col - 1, above);
		setVertSideCounter(row - 1, col + 1, under);

	}

	private void frontClock() {

		char side[] = getHorSide(5, 3);
		setHorSideCounter(5, 3, getVertSide(6, 2));
		setVertSide(6, 2, getHorSide(9, 3));
		setHorSideCounter(9, 3, getVertSide(6, 6));
		setVertSide(6, 6, side);
		turnMiddleClock(7, 4);

	}

	private void frontCounter() {
		char side[] = getHorSide(5, 3);
		setHorSide(5, 3, getVertSide(6, 6));
		setVertSideCounter(6, 6, getHorSide(9, 3));
		setHorSide(9, 3, getVertSide(6, 2));
		setVertSideCounter(6, 2, side);
		turnMiddleCounter(7, 4);

	}

	private void topClock() {
		char side[] = getHorSide(2, 3);
		setHorSideCounter(2, 3, getHorSide(6, 0));
		setHorSide(6, 0, getHorSide(6, 3));
		setHorSide(6, 3, getHorSide(6, 6));
		setHorSideCounter(6, 6, side);

		turnMiddleClock(4, 4);

	}

	private void topCounter() {
		char side[] = getHorSide(2, 3);
		setHorSideCounter(2, 3, getHorSide(6, 6));
		setHorSide(6, 6, getHorSide(6, 3));
		setHorSide(6, 3, getHorSide(6, 0));
		setHorSideCounter(6, 0, side);

		turnMiddleCounter(4, 4);

	}

	private void backClock() {

		char side[] = getHorSide(11, 3);
		setHorSide(11, 3, getVertSide(6, 0));
		setVertSideCounter(6, 0, getHorSide(3, 3));
		setHorSide(3, 3, getVertSide(6, 8));
		setVertSideCounter(6, 8, side);
		turnMiddleClock(1, 4);

	}

	private void backCounter() {

		char side[] = getHorSide(11, 3);
		setHorSideCounter(11, 3, getVertSide(6, 8));
		setVertSide(6, 8, getHorSide(3, 3));
		setHorSideCounter(3, 3, getVertSide(6, 0));
		setVertSide(6, 0, side);

		turnMiddleCounter(1, 4);

	}

	private void bottomClock() {
		char side[] = getHorSide(8, 3);
		setHorSide(8, 3, getHorSide(8, 0));
		setHorSideCounter(8, 0, getHorSide(0, 3));
		setHorSideCounter(0, 3, getHorSide(8, 6));
		setHorSide(8, 6, side);

		turnMiddleClock(10, 4);

	}

	private void bottomCounter() {
		char side[] = getHorSide(8, 3);
		setHorSide(8, 3, getHorSide(8, 6));
		setHorSideCounter(8, 6, getHorSide(0, 3));
		setHorSideCounter(0, 3, getHorSide(8, 0));
		setHorSide(8, 0, side);

		turnMiddleCounter(10, 4);

	}

	private void leftClock() {
		char side[] = getVertSide(6, 3);
		setVertSide(6, 3, getVertSide(3, 3));
		setVertSide(3, 3, getVertSide(0, 3));
		setVertSide(0, 3, getVertSide(9, 3));
		setVertSide(9, 3, side);

		turnMiddleClock(7, 1);

	}

	private void leftCounter() {
		char side[] = getVertSide(6, 3);
		setVertSide(6, 3, getVertSide(9, 3));
		setVertSide(9, 3, getVertSide(0, 3));
		setVertSide(0, 3, getVertSide(3, 3));
		setVertSide(3, 3, side);

		turnMiddleCounter(7, 1);

	}

	private void rightClock() {

		char side[] = getVertSide(6, 5);
		setVertSide(6, 5, getVertSide(9, 5));
		setVertSide(9, 5, getVertSide(0, 5));
		setVertSide(0, 5, getVertSide(3, 5));
		setVertSide(3, 5, side);

		turnMiddleClock(7, 7);
	}

	private void rightCounter() {
		char side[] = getVertSide(6, 5);
		setVertSide(6, 5, getVertSide(3, 5));
		setVertSide(3, 5, getVertSide(0, 5));
		setVertSide(0, 5, getVertSide(9, 5));
		setVertSide(9, 5, side);

		turnMiddleCounter(7, 7);

	}

	private class Twister {
		private Twist twist[] = new Twist[12];

		private Twister() {
			twist[FRONT*2] = new FrontClock();
			twist[FRONT*2+1] = new FrontCounter();
			twist[TOP*2] = new TopClock();
			twist[TOP*2+1] = new TopCounter();
			twist[BACK*2] = new BackClock();
			twist[BACK*2+1] = new BackCounter();
			twist[BOTTOM*2] = new BottomClock();
			twist[BOTTOM*2+1] = new BottomCounter();
			twist[LEFT*2] = new LeftClock();
			twist[LEFT*2+1] = new LeftCounter();
			twist[RIGHT*2] = new RightClock();
			twist[RIGHT*2+1] = new RightCounter();
		}

		public void twist(int i) {
			twist[i].twist();
		}

		private abstract class Twist {
			public abstract void twist();
		}

		public class FrontClock extends Twist {

			@Override
			public void twist() {
				frontClock();
			}

		}

		public class FrontCounter extends Twist {

			@Override
			public void twist() {
				frontCounter();
			}

		}

		public class TopClock extends Twist {

			@Override
			public void twist() {
				topClock();
			}

		}

		public class TopCounter extends Twist {

			@Override
			public void twist() {
				topCounter();
			}

		}

		public class BackClock extends Twist {

			@Override
			public void twist() {
				backClock();
			}

		}

		public class BackCounter extends Twist {

			@Override
			public void twist() {
				backCounter();
			}

		}

		public class BottomClock extends Twist {

			@Override
			public void twist() {
				bottomClock();
			}

		}

		public class BottomCounter extends Twist {

			@Override
			public void twist() {
				bottomCounter();
			}

		}

		public class LeftClock extends Twist {

			@Override
			public void twist() {
				leftClock();
			}

		}

		public class LeftCounter extends Twist {

			@Override
			public void twist() {
				leftCounter();
			}

		}

		public class RightClock extends Twist {

			@Override
			public void twist() {
				rightClock();
			}

		}

		public class RightCounter extends Twist {

			@Override
			public void twist() {
				rightCounter();
			}

		}

	}

	public static void main(String[] args) {
		Cube_OLD cube = new Cube_OLD();
		System.out.println(cube.toString());
		// System.out.println(cube.toStringCoordinates());

		// if (cube.isSolved())
		// System.out.println("Solved!");
		//
//		cube.twist(FRONT);
		
		System.out.println(cube.toString());


		// cube.twist(3);
		//
		// System.out.println(cube.toString());
		//
		// if (!cube.isSolved())
		// System.out.println("Not Solved!");

	}

}
